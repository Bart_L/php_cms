<?php
/**
 * Created by PhpStorm.
 * User: bartek
 * Date: 18.11.2017
 * Time: 17:35
 */

include('../model/db_connect.php');

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $statement = $mysqli->prepare("DELETE FROM articles WHERE id = ? LIMIT 1");
    $statement->bind_param("i",$id);
    $statement->execute();
    $statement->close();
    header("Location: ../index.php");
}