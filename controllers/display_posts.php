<?php
/**
 * Created by PhpStorm.
 * User: Bartosz Ligęza
 * Date: 18.11.2017
 * Time: 18:21
 */

include('model/db_connect.php');
//zapytanie do bazy o tablicę articles i przypisanie wyniku do zmiennej result
$result = $mysqli->query("SELECT * FROM articles ORDER BY id");
//pętla przypisująca do zmiennej article pojedynczy wiersz z tablicy result przy każdym przejsciu pętli
while ($article = mysqli_fetch_array($result)) {
    //wyprowadzanie dynamiczne na ekran htmla z zawartością z php
    echo '<article class="single-article">';
    echo '<h3>' . $article['title'] . '</h3>';
    echo '<img src="' . $article['image'] . '" alt="" >';
    echo '<div class="article-content">';
    echo '<p>' . $article['content'] . '</p>';
    echo '</div>';
    echo '<a href="controllers/delete_post.php?id=' . $article['id'] . '">';
    echo '<div class="ui label"><i class="remove icon"></i>Usuń</div>';
    echo '</a>';
    echo '</article>';
}
?>