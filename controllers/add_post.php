<?php
/**
 * Created by PhpStorm.
 * User: Bartosz Ligęza
 * Date: 18.11.2017
 * Time: 18:27
 */

include('model/db_connect.php');

if (isset( $_POST['add'] )) {
    //strip_tags spowoduje usunięcie ewentualnych znaczników wpisanych do pól przez użytkownika
    $title = strip_tags($_POST['title']);
    $content = strip_tags($_POST['content']);
    $image = strip_tags($_POST['image']);

    //metoda prepare ma za zadanie przygotować statement do wysłania do bazy, wywołuje się obiektowo na zmiennej od zapytania bazy ($mysqli)
    $statement = $mysqli->prepare("INSERT articles (title,image,content) VALUES (?,?,?)");
    //bindowanie parametrow do poprzednio przygotowanego wyrażenia
    $statement->bind_param("sss", $title,$image,$content);
    //wykonanie wstawiania
    $statement->execute();
    //zamykanie wyrażenia
    $statement->close();
    //przekierowanie do index.php zeby odświeżyć stronę i wyczyscić post request z nagłówka
    header("Location: index.php");
}