<?php
/**
 * Created by PhpStorm.
 * User: Bartosz Ligęza
 * Date: 19.11.2017
 * Time: 10:20
 */

include('model/db_connect.php');

$result = $mysqli->query("SELECT * FROM users ORDER BY id");
while ($user = mysqli_fetch_array($result)) {

    echo '<tr>';
        echo '<td>' . $user['id'] . '</td>';
        echo '<td>' . $user['first_name'] . '</td>';
        echo '<td>' . $user['last_name'] . '</td>';
        echo '<td>' . $user['email'] . '</td>';
        echo '<td>' . $user['university'] . '</td>';
        echo '<td>' . $user['job'] . '</td>';
    echo '</tr>';
}

?>