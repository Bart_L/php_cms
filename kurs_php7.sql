-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 19 Lis 2017, 10:36
-- Wersja serwera: 10.1.26-MariaDB
-- Wersja PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `kurs_php7`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `image` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `articles`
--

INSERT INTO `articles` (`id`, `title`, `image`, `content`) VALUES
(4, 'Podróże po USA', 'http://via.placeholder.com/350x150', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque odio eros, fermentum sit amet congue et, porttitor volutpat lectus. Praesent interdum erat sed lectus dignissim euismod. Suspendisse quis neque molestie, fringilla sapien ut, congue elit. Ut dignissim viverra sollicitudin. Phasellus sit amet egestas justo. Aliquam neque nisl, elementum ac ultricies ac, molestie at metus. Vivamus lorem augue, consequat eu posuere vitae, facilisis eu eros. Aliquam ac nisl vel ligula pulvinar hendrerit.'),
(5, 'Przepis na zupę PHO', 'http://via.placeholder.com/350x150', 'Nam varius, est at suscipit facilisis, eros lectus malesuada lacus, a pulvinar lectus sem vitae leo. Maecenas mauris turpis, facilisis vel lorem ac, porttitor rutrum neque. Nullam dignissim placerat pulvinar. Quisque efficitur enim nec viverra condimentum. Duis vehicula enim purus, id lacinia magna tincidunt vel. Donec in lectus vel ante venenatis malesuada sed ac ante. Maecenas accumsan et ex egestas porta. In laoreet arcu dignissim lectus pretium, sit amet condimentum mauris tincidunt. Ut condimentum justo eget lorem fringilla, at viverra mi tempor.'),
(6, 'Relacja z festiwalu muzycznego', 'http://via.placeholder.com/350x150', 'Nam et augue volutpat, euismod ligula vel, rutrum enim. Suspendisse maximus ac orci vel elementum. Sed hendrerit vitae lectus eu gravida. Phasellus mauris mauris, lobortis at est quis, mattis ornare orci. Donec erat mauris, porta varius libero eu, imperdiet pharetra nulla. Vestibulum interdum lorem dui, sed tristique eros sagittis et. Proin et ipsum congue, bibendum nibh sit amet, auctor lorem. Duis hendrerit sem et diam imperdiet, quis commodo dui posuere. Nam id facilisis dui. Proin elementum, erat in sagittis facilisis, magna est fringilla libero, non efficitur enim nunc non risus. Nullam massa elit, blandit eget turpis sit amet, fringilla maximus libero. Aenean sed mauris non lacus sodales tempor. Ut ullamcorper erat leo, vitae gravida ligula mollis id. Pellentesque sit amet vestibulum ex. Praesent bibendum id sapien in feugiat. Sed rutrum id turpis vel interdum.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email` text NOT NULL,
  `university` text NOT NULL,
  `job` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `university`, `job`) VALUES
(1, 'Lucian', 'Larkings', 'llarkings0@phpbb.com', 'Technical University of Poznan', 'Systems Administrator II'),
(2, 'Therese', 'Gaine of England', 'tgaineofengland1@dailymail.co.uk', 'Sarajevo Film Academy', 'Senior Editor'),
(3, 'Elston', 'Hinkens', 'ehinkens2@phpbb.com', 'Ecole des Hautes Etudes Commerciales du Nord', 'Web Designer I'),
(4, 'Wanda', 'Koenraad', 'wkoenraad3@house.gov', 'Rikkyo University (St. Paul\'s University)', 'Programmer IV'),
(5, 'Corissa', 'Charer', 'ccharer4@nymag.com', 'Universidad Gran Mariscal de Ayacucho', 'Marketing Assistant'),
(6, 'Cory', 'Corselles', 'ccorselles5@berkeley.edu', 'Xuzhou Normal University', 'Biostatistician IV'),
(7, 'Al', 'Shawl', 'ashawl6@sphinn.com', 'Kent State University - East Liverpool', 'Technical Writer'),
(8, 'Hale', 'Aleevy', 'haleevy7@ameblo.jp', 'Karnataka State Law University', 'Tax Accountant'),
(9, 'Danit', 'Alvarado', 'dalvarado8@nasa.gov', 'Osaka Women\'s University', 'Financial Analyst'),
(10, 'Kermit', 'Luggar', 'kluggar9@si.edu', 'Instituto Tecnológico Metropolitano', 'Social Worker'),
(11, 'Charlie', 'Tipping', 'ctippinga@xinhuanet.com', 'Instituto Superior D. Afonso III - INUAF', 'Editor'),
(12, 'Maegan', 'Petren', 'mpetrenb@pinterest.com', 'Mashhad University of Medical Sciences', 'VP Sales'),
(13, 'Christen', 'Crosswaite', 'ccrosswaitec@dyndns.org', 'Hokkaido Tokai University', 'Quality Engineer'),
(14, 'Quintina', 'Sandle', 'qsandled@usgs.gov', 'Armstrong Atlantic State University', 'Financial Analyst'),
(15, 'Ellsworth', 'Guy', 'eguye@vk.com', 'Trinity College Carmarthen', 'Associate Professor'),
(16, 'Gabi', 'Seagrave', 'gseagravef@tumblr.com', 'EHSAL - Europese Hogeschool Brussel', 'Pharmacist'),
(17, 'Nero', 'Birkmyre', 'nbirkmyreg@nymag.com', 'University of Memphis', 'VP Marketing'),
(18, 'Mehetabel', 'Adam', 'madamh@prnewswire.com', 'Universidade Vale do Rio Doce', 'Desktop Support Technician'),
(19, 'Staffard', 'Marchant', 'smarchanti@w3.org', 'Brooks Institute of Photography', 'Assistant Professor'),
(20, 'Linnea', 'Scarasbrick', 'lscarasbrickj@feedburner.com', 'University of Bridgeport', 'Chief Design Engineer'),
(21, 'Janean', 'Gillie', 'jgilliek@omniture.com', 'Universitas Mahasaraswati Denpasar', 'Compensation Analyst'),
(22, 'Conrad', 'Golland', 'cgollandl@eepurl.com', 'University of Alabama - Tuscaloosa', 'Accountant IV'),
(23, 'Franklin', 'Tunuy', 'ftunuym@disqus.com', 'Ithaca College', 'VP Quality Control'),
(24, 'Dorie', 'Bellinger', 'dbellingern@nifty.com', 'Ashiya University', 'Clinical Specialist'),
(25, 'Letisha', 'Kaman', 'lkamano@dyndns.org', 'St. Anthony College of Nursing', 'Senior Quality Engineer'),
(26, 'Renata', 'Gallie', 'rgalliep@mozilla.com', 'Hochschule Darmstadt', 'Financial Analyst'),
(27, 'Freddy', 'Collumbell', 'fcollumbellq@live.com', 'Columbia International University', 'Web Designer II'),
(28, 'Marianne', 'Ings', 'mingsr@sfgate.com', 'Savannah State University', 'Programmer Analyst IV'),
(29, 'Gerhardine', 'Pratte', 'gprattes@senate.gov', 'Polish-Japanese Institute of Information Technology in Warsaw', 'Accountant III'),
(30, 'Fonsie', 'Adame', 'fadamet@prnewswire.com', 'Westminster International University in Tashkent', 'Office Assistant III'),
(31, 'Felic', 'Welford', 'fwelfordu@technorati.com', 'Modern Sciences & Arts University', 'Chief Design Engineer'),
(32, 'Leeland', 'Drinkeld', 'ldrinkeldv@princeton.edu', 'Brenau University', 'Web Developer IV'),
(33, 'Paddie', 'Setterthwait', 'psetterthwaitw@technorati.com', 'Tokyo Gakugei University', 'Environmental Tech'),
(34, 'Eal', 'McAloren', 'emcalorenx@usgs.gov', 'Zanjan University of Medical Sciences', 'Civil Engineer'),
(35, 'Annabella', 'Conisbee', 'aconisbeey@dailymail.co.uk', 'University of Oregon', 'VP Quality Control'),
(36, 'Isobel', 'Archdeacon', 'iarchdeaconz@geocities.com', 'Wilkes University', 'Junior Executive'),
(37, 'Mychal', 'Durrance', 'mdurrance10@nps.gov', 'Al Mansour University College', 'Director of Sales'),
(38, 'Carlie', 'Gres', 'cgres11@mapquest.com', 'Academy of Art College', 'Senior Financial Analyst'),
(39, 'Klarrisa', 'Sharplin', 'ksharplin12@indiatimes.com', 'Hobe Sound Bible College', 'Registered Nurse'),
(40, 'Bayard', 'Battams', 'bbattams13@spotify.com', 'Takasaki City University of Economics', 'Actuary'),
(41, 'Ewell', 'Kildahl', 'ekildahl14@netscape.com', 'Indiana Wesleyan University', 'VP Accounting'),
(42, 'Jermayne', 'Mallon', 'jmallon15@sohu.com', 'Uganda Christian University', 'Operator'),
(43, 'Hube', 'Chater', 'hchater16@walmart.com', 'Help University College', 'Quality Engineer'),
(44, 'Jsandye', 'Napper', 'jnapper17@ted.com', 'Universidad de Puerto Rico, Carolina', 'Staff Accountant I'),
(45, 'Goddard', 'Aiskrigg', 'gaiskrigg18@paypal.com', 'University of Science & Technology Bannu', 'Senior Cost Accountant'),
(46, 'Ceciley', 'Nixon', 'cnixon19@cmu.edu', 'Jilin Agricultural University', 'Compensation Analyst'),
(47, 'Flin', 'Goodnow', 'fgoodnow1a@theglobeandmail.com', 'Universidade Estadual Paulista', 'Developer IV'),
(48, 'Harbert', 'Clabburn', 'hclabburn1b@toplist.cz', 'Rhode Island College', 'Statistician I'),
(49, 'Julie', 'Quarlis', 'jquarlis1c@usa.gov', 'Ho Chi Minh City University of Architecture', 'Nurse'),
(50, 'Maxy', 'Barbery', 'mbarbery1d@businessinsider.com', 'Kigali Institute of Science & Technology', 'Statistician III');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
