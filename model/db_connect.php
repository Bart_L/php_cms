<?php
/**
 * Created by PhpStorm.
 * User: Bartosz Ligęza
 * Date: 18.11.2017
 * Time: 14:24
 */

$dbServer = 'localhost';
$dbUser = 'root';
$dbPassword = '';
$dbName = 'kurs_php7';
$mysqli = new mysqli($dbServer, $dbUser, $dbPassword, $dbName);

$mysqli->set_charset("utf8");
if (mysqli_connect_errno()) {
    echo 'Błąd połączenia z bazą danych';
}